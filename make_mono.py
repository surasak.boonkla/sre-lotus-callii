import re
import numpy as np
from scipy.io import wavfile
import os
import glob

root_dir = '/opt/LotusCaLLiiStereo'
target_dir = '/opt/LotusCaLLiiMono'
sources = ['S1', 'S2', 'S3', 'S4']

for source in sources:
    for fname in glob.iglob(f'{root_dir}/{source}/dev/*/*.wav'):
        tokens = re.split(r'_|\-|\\|\.|/',fname)
        sid = tokens[-9]
        startSample = tokens[-3]
        endSample = tokens[-2]
        #print([int(startSample), int(endSample)])
        aid = f'{tokens[-7][0:-1]}'
        arole = tokens[-7][-1]
        cid = f'{tokens[-5][0:-1]}'
        crole = tokens[-5][-1]
        dialogID = tokens[-4]
        #print(f'CID:{cid}-{crole}, AID:{aid}-{arole}, Folder:{cid}, using dialog {dialogID} ')
        fs, data = wavfile.read(fname)
        destPathC = f'{target_dir}/{source}/dev/{source}{cid}'
        if source == 'S4':
            tmp = list(aid)
            tmp[1] = '1'
            aid = "".join(tmp)
        destPathA = f'{target_dir}/{source}/dev/{source}{aid}'
        if not os.path.exists(destPathA):
            os.system(f'mkdir -p {destPathA}')
            print(f'Make folder {destPathA}')
        if not os.path.exists(destPathC):
            os.system(f'mkdir -p {destPathC}')
            print(f'Make folder {destPathC}')
        destFile = f'{source}-{aid}{arole}_{source}-{cid}{crole}_{dialogID}_{startSample}_{endSample}.wav'
        wavfile.write(f'{destPathA}/{destFile}', fs, data[:,0])
        wavfile.write(f'{destPathC}/{destFile}', fs, data[:,1])
