import random

with open('data/callii_test/wav.scp') as fd:
    lines = fd.readlines()

file_list = []
for line in lines:
    tokens = re.split(r' ',line.strip())
    file_list.append(tokens[0])

fd = open('data/callii_test/trials', 'wt')
for i in range(len(file_list)-2):
    j = random.sample(range(len(file_list)),1)
    fl1 = file_list[i]
    fl2 = file_list[i+1]
    tokens = re.split(r'-', fl1)
    id1 = tokens[0]
    tokens = re.split(r'-', fl2)
    id2 = tokens[0]

    if id1 == id2:
        fd.write(f'{fl1} {fl2} target\n')

        fl2 = file_list[j[0]]
        tokens = re.split(r'-', fl2)
        id2 = tokens[0]
        if not id1 == id2:
            fd.write(f'{fl1} {fl2} nontarget\n')

fd.close()
